from random import choice


NIEBIESKI = 1
ZIELONY = 2
CZERWONY = 3
FIOLETOWY = 4
NATURALNY = 5

CHATA = 0

DREWNIANA = 1
GLINIANA = 2
KAMIENNA = 3

KOLORY = (
    NIEBIESKI,
    ZIELONY,
    CZERWONY,
    FIOLETOWY,
    NATURALNY,
)

FAZA_I_ROZPOCZECIE = 1
FAZA_II_UZUPELNIENIE = 2
FAZA_III_PRACA = 3
FAZA_IV_POWROT = 4

class Izba(object):
    def __init__(self, typ=DREWNIANA):
        self.typ = typ


class KartaRundy(object):
    def __init__(self, runda):
        self.runda = runda

class CzlonekRodziny(object):
    def __init__(self, kolor):
        self.kolor = kolor

class Obora(object):
    def __init__(self, kolor):
        self.kolor = kolor
        
class Ogrodzenie(object):
    def __init__(self, kolor):
        self.kolor = kolor

class Pole(object):
    def __init__(self, typ=None):
        self.typ = typ
        self.zetony = []
        
class Gospodarstwo(object):
    def __init__(self):
        self.pola = [Pole() for i in range(15)]
        self.pola[5].typ = CHATA
        self.pola[5].zetony.append(Izba())
        self.pola[10].typ = CHATA
        self.pola[10].zetony.append(Izba())
        
class ZasobyOgolny(object):
    def __init__(self, kolor):
        self.czlonkowie_rodziny = [CzlonekRodziny(kolor) for x in range(5)]
        self.obory = [Obora(kolor) for x in range(4)]
        self.ogrodzenia = [Ogrodzenie(kolor) for x in range(15)]

class Gracz(object):
    def __init__(self, kolor):
        self.kolor = kolor
        self.rozpoczynajacy = False
        self.zywnosc = 0
        self.gospodarstwo = Gospodarstwo()
        self.zasoby_ogolne = ZasobyOgolny(kolor)
    
class Gra(object):
    def __init__(self):
        self.dostepne_kolory = list(KOLORY)
        self.gracze = []
        self.akcje = []
        self.karty_rundy = [KartaRundy for i in range(14)]
            
    def nowy_gracz(self, kolor=None):
        if kolor == None:
            kolor = choice(self.dostepne_kolory)
            self.dostepne_kolory.remove(kolor)
            
        gracz = Gracz(kolor)
        self.gracze.append(gracz)
        return gracz
    
    def start(self, gracz_rozpoczynajacy):
        for gracz in self.gracze:
            gracz.rozpoczynajacy = (gracz == gracz_rozpoczynajacy)
            gracz.zywnosc = 2 if gracz.rozpoczynajacy else 3

        
if __name__ == '__main__':
    gra = Gra()
    graczy = 3
    for nr in range(graczy):
        gra.nowy_gracz()
    gra.start(choice(gra.gracze))
    
    for gracz in gra.gracze:
        print gracz.kolor, gracz.rozpoczynajacy, gracz.zywnosc
    print ''
        
    for i, pole in enumerate(gra.gracze[0].gospodarstwo.pola):
        print pole.typ, '\t',
        if i%5 == 4:
            print ''
